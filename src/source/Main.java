package source;

import java.io.IOException;

import comptabilite.Filliales;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import vueprincipale.EcranControlleur;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;

public class Main extends Application
{
	
	private static Stage primaryStage;
	private static EcranControlleur lEcranControlleur;

	// Point d'entr�e de l'application
	public static void main(String[] args)
	{
		new Filliales("Roubaix", true);
		new Filliales("Dunkerque", false);
		new Filliales("Lille", true);
		launch(args);
	}

	// M�thode impos�e par l'h�ritage de la classe Application
	@Override
	public void start(Stage lePrimaryStage)
	{
		primaryStage = lePrimaryStage;
		primaryStage.setTitle("Gestions des employ� de l'entreprise Xincorportated.");
		showFenetreBase();
	}

	// Vue Gestion des contacts
	public void showFenetreBase()
	{
		try
			{
			BorderPane menuLayout;
			FXMLLoader myFXMLloader = new FXMLLoader();
			myFXMLloader.setLocation(Main.class.getResource("../vueprincipale/Ecran.fxml"));
			
			menuLayout = myFXMLloader.load();
			Scene scene = new Scene(menuLayout);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setResizable(false);
			
			// D�claration et instanciation du controller, passage d'une
			// r�f�rence sur l'instance de la classe MenuApp (this)
			EcranControlleur.setMain(this);
			
			// D�marrage du Stage
			primaryStage.show();
			}
			catch (IOException e)
			{
				System.out.println("Stage Show Bug, use  e.printStackTrace();  ");
			}
	}
	
	// accesseurs
	public static Stage getPrimaryStage()
	{
		return primaryStage;
	}

	public static void setEcranControlleur(EcranControlleur unEcranControlleur)
	{

		lEcranControlleur = unEcranControlleur;
	}

	public static EcranControlleur getlEcranControlleur()
	{
		return lEcranControlleur;
	}
	
	
}
