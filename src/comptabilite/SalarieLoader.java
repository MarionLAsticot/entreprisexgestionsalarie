package comptabilite;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import fm.ml.outils.OutilsDate;
import fm.ml.outils.OutilsGerantFichier;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import source.Main;
import vueprincipale.EcranControlleur;

public abstract class SalarieLoader
{
	private static int fillialleDernierSalarie = 0;
	private static int placeDernierSalarie = 0;

	private SalarieLoader()
	{
	}

	public static void getSalarier(List<Filliales> fillialesARemplir)
	{
		Alert alert = new Alert(AlertType.ERROR, "Ce fichier ne correspond pas aux normes.");
		List<String> laListeDesSalarie = new ArrayList<>();
		laListeDesSalarie.add("");
		laListeDesSalarie = OutilsGerantFichier.chargerFichierEtUri(Main.getPrimaryStage(), "Echec de chargement.");
		while (!laListeDesSalarie.get(0).equals("Echec de chargement.")
				&& !laListeDesSalarie.get(1).equals("Type|Sexe|Pr�nom|DateNaissance|Nom|Agence|DateEmbauche"))
		{
			Optional<ButtonType> result = alert.showAndWait();
			if (result.isPresent() && result.get() == ButtonType.OK)
			{
				laListeDesSalarie = OutilsGerantFichier.chargerFichierEtUri(Main.getPrimaryStage(),
						"Echec de chargement.");
			}
		}
		if (!laListeDesSalarie.get(0).equals("Echec de chargement."))
		{
			String lURI = laListeDesSalarie.get(0);
			laListeDesSalarie.remove(0);
			trierSalarier(fillialesARemplir, laListeDesSalarie, lURI);
		}
	}

	private static void trierSalarier(List<Filliales> fillialesARemplir, List<String> laListeDesSalarie, String lURI)
	{
		boolean isFemme;
		String prenom;
		String dateNaissance;
		String dateEmbauche;
		String agence;
		String nom;
		String type;
		int position;
		String texteEnCours;

		for (int i = 1; i < laListeDesSalarie.size(); i++)
		{
			texteEnCours = laListeDesSalarie.get(i);
			position = texteEnCours.indexOf('|');
			type = texteEnCours.substring(0, position);

			texteEnCours = texteEnCours.substring(position + 1);
			position = texteEnCours.indexOf('|');
			if (texteEnCours.substring(0, position).equals("2"))
				isFemme = true;
			else
				isFemme = false;

			texteEnCours = texteEnCours.substring(position + 1);
			position = texteEnCours.indexOf('|');
			prenom = texteEnCours.substring(0, position);

			texteEnCours = texteEnCours.substring(position + 1);
			position = texteEnCours.indexOf('|');
			if (position != -1)
				dateNaissance = texteEnCours.substring(0, position);
			else
				dateNaissance = texteEnCours.substring(0);

			if (type.equals("Salari�"))
			{
				texteEnCours = texteEnCours.substring(position + 1);
				position = texteEnCours.indexOf('|');
				nom = texteEnCours.substring(0, position).toUpperCase();

				texteEnCours = texteEnCours.substring(position + 1);
				position = texteEnCours.indexOf('|');
				agence = texteEnCours.substring(0, position);

				texteEnCours = texteEnCours.substring(position + 1);
				dateEmbauche = texteEnCours.substring(0);

				creerSalarier(fillialesARemplir, isFemme, prenom, dateNaissance, nom, agence, dateEmbauche);
			}
			if (type.equals("Enfant"))
			{
				creerEnfant(isFemme, prenom, dateNaissance);
			}
		}

		EcranControlleur.lAdresse = lURI;
		EcranControlleur.quandElementChargerStatic();

	}

	private static void creerEnfant(boolean isFemme, String prenom, String dateNaissance)
	{
		Filliales.getLesFilliales().get(fillialleDernierSalarie).getSalariesEmbaucher().get(placeDernierSalarie)
				.getEnfants().add(new Humain(isFemme, prenom, OutilsDate.stringToDate(dateNaissance)));
	}

	private static void creerSalarier(List<Filliales> fillialesARemplir, boolean isFemme, String prenom,
			String dateNaissance, String nom, String agence, String dateEmbauche)
	{
		for (int i = 0; i < fillialesARemplir.size(); i++)
		{
			if (fillialesARemplir.get(i).getLeNom().equals(agence))
			{
				Salarie temp = new Salarie(isFemme, prenom, OutilsDate.stringToDate(dateNaissance), nom, agence,
						OutilsDate.stringToDate(dateEmbauche));

				Filliales.getLesFilliales().get(i).getSalariesEmbaucher().add(temp);

				fillialleDernierSalarie = i;

				placeDernierSalarie = Filliales.getLesFilliales().get(i).getSalariesEmbaucher().size() - 1;
			}
		}
	}
}
