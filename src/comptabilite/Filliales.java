package comptabilite;

import java.util.ArrayList;
import java.util.List;

public class Filliales
{
	private static List<Filliales> lesFilliales = new ArrayList<>();
	private String leNom;
	private boolean asUnRestaurant;
	private List<Salarie> salariesEmbaucher = new ArrayList<>();
	
	public Filliales(String leNom, boolean asUnRestaurant) 
	{
		this.leNom = leNom;
		this.asUnRestaurant = asUnRestaurant;
		lesFilliales.add(this);
	}

	public List<Salarie> getSalariesEmbaucher()
	{
		return salariesEmbaucher;
	}

	public void setSalariesEmbaucher(List<Salarie> salariesEmbaucher)
	{
		this.salariesEmbaucher = salariesEmbaucher;
	}

	public static List<Filliales> getLesFilliales()
	{
		return lesFilliales;
	}

	public String getLeNom()
	{
		return leNom;
	}

	public boolean isAsUnRestaurant()
	{
		return asUnRestaurant;
	}
	
	@Override
	public String toString() 
	{
		return leNom;
	}
	
}
