package comptabilite;

import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.print.PrinterJob;

import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.Alert.AlertType;
import source.Main;

public abstract class Imprimeur
{
	private static List<Salarie> laListeDEmployer = new ArrayList<>();

	private Imprimeur()
	{
	}

	public static void imprime(ListView<String> lesEmployer, String leNomDEntreprise)
	{
		ObservableList<String> laListe = FXCollections.observableArrayList(lesEmployer.getItems());
		laListe.remove(0);
		for (int i = 0; i < laListe.size(); i++)
		{
			transformerEmployer(laListe.get(i));
		}
		
		PrinterJob job = PrinterJob.createPrinterJob();
		job.showPrintDialog(Main.getPrimaryStage());
		if (job != null)
		{
			boolean success = true;
				success = job.printPage(createListView(leNomDEntreprise));
			
			if (success)
			{
				job.endJob();
			}
			else 
			{
				Alert alert = new Alert(AlertType.INFORMATION, "L'impression � �chou�e.");
				alert.showAndWait();
			}
		}
	}

	private static ListView<String> createListView(String leNomDEntreprise)
	{
		ListView<String> laListe = new ListView<>();
		List<String> getTheStrings = new ArrayList<>();
		if (leNomDEntreprise.equals("Afficher tout"))
		{
			getTheStrings.add("Dans toutes les entreprises");
		}
		else
		{
			getTheStrings.add("Dans l'entreprise " + leNomDEntreprise);
		}
		getTheStrings.add("Nom            |Prenom         |Embauch� le|Bonus vacances|Cheques noels");
		for (int i = 0; i < laListeDEmployer.size(); i++)
		{
			Salarie employer = laListeDEmployer.get(i);
			String chequeVacance = "";
			if (employer.calculerMesDroits()[1] == 0)
				chequeVacance = " Aucun        ";
			else
				chequeVacance = " Disponibles  ";
			String monString = String.format("%-15s|%-15s|%2d/%2d/%2d |%s|%5d �",
					employer.getNom(), employer.getPrenom(), employer.getDateDEmbauche().getDayOfMonth(),
					employer.getDateDEmbauche().getMonthValue(), employer.getDateDEmbauche().getYear(), chequeVacance, employer.calculerMesDroits()[0]);
			getTheStrings.add(monString);
		}
		laListe.setItems(FXCollections.observableList(getTheStrings));
		laListe.setPrefWidth(600);
		laListe.setMinHeight((double)(laListeDEmployer.size()*14));
		laListe.setStyle("-fx-font-family: \"monospace\"; -fx-font-size: 11px;");
		return laListe;
	}

	private static void transformerEmployer(String string)
	{
		for (Filliales laFilliale : Filliales.getLesFilliales())
		{
			for (int i = 0; i < laFilliale.getSalariesEmbaucher().size(); i++)
			{
				if (laFilliale.getSalariesEmbaucher().get(i).toString().equals(string))
				{
					laListeDEmployer.add(laFilliale.getSalariesEmbaucher().get(i));
				}
			}

		}
	}

}
