package comptabilite;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import calculeurdedroits.CalculDeDroits;

public class Salarie extends Humain
{
	private String nom;
	private String agence;
	private LocalDate dateDEmbauche;
	private ArrayList<Humain> enfants = new ArrayList<>();

	/**
	 * @param femme
	 * @param prenom
	 * @param dateDeNaissance
	 * @param nom
	 * @param agence
	 * @param dateEmbauche
	 */
	public Salarie(boolean femme, String prenom, LocalDate dateDeNaissance, String nom, String agence,
			LocalDate dateDEmbauche)
	{
		super(femme, prenom, dateDeNaissance);
		this.nom = nom.toUpperCase().trim();
		this.agence = agence;
		this.dateDEmbauche = dateDEmbauche;
	}

	public String getNom()
	{
		return nom;
	}

	public void setNom(String nom)
	{
		this.nom = nom;
	}

	public String getAgence()
	{
		return agence;
	}

	public void setAgence(String agence)
	{
		this.agence = agence;
	}

	public LocalDate getDateDEmbauche()
	{
		return dateDEmbauche;
	}

	public void setDateDEmbauche(LocalDate dateDEmbauche)
	{
		this.dateDEmbauche = dateDEmbauche;
	}

	public List<Humain> getEnfants()
	{
		return enfants;
	}

	public void setEnfants(List<Humain> enfants)
	{
		this.enfants = (ArrayList<Humain>) enfants;
	}

	/**
	 * Retourne un tableaux de deux int: <br/>
	 * [1] : le nombre d'euro de cheques noels autoris�. [2] : 0 s'il n'y � pas de
	 * cheques vacances, 1 s'il y �.
	 * 
	 * @return
	 */
	public int[] calculerMesDroits()
	{
		return CalculDeDroits.calculerLesDroits(this);
	}

	@Override
	public String toString()
	{
		return nom + " " + getPrenom();
	}

	public static boolean checkDatePossibleEnfant(Salarie salarieChoisit, LocalDate age)
	{
		if (age != null)
		{
		boolean isOk = true;
		int laDifference;
			laDifference = (salarieChoisit.getDateDeNaissance().getYear() * 12)
					+ (salarieChoisit.getDateDeNaissance().getMonthValue());
			laDifference = ((age.getYear() * 12) + (age.getMonthValue())) - laDifference;
			System.out.println(laDifference);
			if (laDifference < 192)
				isOk = false;
			for (int i = 0; i < salarieChoisit.getEnfants().size() && isOk; i++)
			{
				laDifference = salarieChoisit.getEnfants().get(i).getDateDeNaissance().getMonthValue()
						- age.getMonthValue();
				laDifference = laDifference + ((salarieChoisit.getEnfants().get(i).getDateDeNaissance().getYear() * 12)
						- (age.getYear() * 12));
				System.out.println("   "+laDifference);
				if (laDifference >= -9 && laDifference <= 9)
				{
					isOk = false;
				}
			}
			return isOk;
		}
		return false;
	}

}
