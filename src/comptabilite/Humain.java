package comptabilite;

import java.time.LocalDate;

public class Humain
{
	private boolean femme;
	private String prenom;
	private LocalDate dateDeNaissance;

	public Humain(boolean femme,String prenom, LocalDate dateDeNaissance) 
	{
		this.femme = femme;
		prenom = prenom.trim();
		prenom = prenom.substring(0,1).toUpperCase() +  prenom.substring(1).toLowerCase() ;
		this.prenom = prenom;
		this.dateDeNaissance = dateDeNaissance;
	}

	public boolean isFemme()
	{
		return femme;
	}

	public void setFemme(boolean femme)
	{
		this.femme = femme;
	}

	public String getPrenom()
	{
		return prenom;
	}

	public void setPrenom(String prenom)
	{
		this.prenom = prenom;
	}

	public LocalDate getDateDeNaissance()
	{
		return dateDeNaissance;
	}

	public void setDateDeNaissance(LocalDate dateDeNaissance)
	{
		this.dateDeNaissance = dateDeNaissance;
	}
	
	@Override
	public String toString() 
	{
		return prenom;
	}
}
