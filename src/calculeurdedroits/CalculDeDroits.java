package calculeurdedroits;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;

import comptabilite.Humain;
import comptabilite.Salarie;

public abstract class CalculDeDroits
{
	private CalculDeDroits()
	{
	}

	public static int[] calculerLesDroits(Salarie leSalarie)
	{
		int[] mesDroits = new int[2];

		mesDroits[0] = droitNoel(leSalarie);
		mesDroits[1] = droitVacances(leSalarie);

		return mesDroits;
	}

	private static int droitNoel(Salarie leSalarie)
	{
		int val = 0;
		for (Humain lEnfant : leSalarie.getEnfants())
		{
			if (Period.between(lEnfant.getDateDeNaissance(), LocalDate.now()).getYears() < 10)
			{
				val = val + 20;
			}
			else if (Period.between(lEnfant.getDateDeNaissance(), LocalDate.now()).getYears() < 15)
			{
				val = val + 30;
			}
			else if (Period.between(lEnfant.getDateDeNaissance(), LocalDate.now()).getYears() < 18)
			{
				val = val + 50;
			}
		}
		return val;
	}

	private static int droitVacances(Salarie leSalarie)
	{
		int val = 0;
		int moisDepuisJuillet = 0;

		int mois = (Period.between(leSalarie.getDateDEmbauche(), LocalDate.now()).getYears())*12;
		mois = mois + ( Period.between(leSalarie.getDateDEmbauche(), LocalDate.now()).getMonths());
		
		if (LocalDate.now().getMonthValue() >= 7)
		{
			moisDepuisJuillet = Period.between(LocalDate.of(LocalDate.now().getYear(), Month.JULY, 1), LocalDate.now())
					.getMonths();
		}
		else
		{
			moisDepuisJuillet = Period
					.between(LocalDate.of(LocalDate.now().getYear() - 1, Month.JULY, 1), LocalDate.now()).getMonths();
		}
		if (mois - moisDepuisJuillet >= 18)
			val = 1;
		return val;
	}
}
