package vueprincipale;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import comptabilite.Filliales;
import comptabilite.Humain;
import comptabilite.Imprimeur;
import comptabilite.Salarie;
import comptabilite.SalarieLoader;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import source.Main;

public class EcranControlleur
{
	private static EcranControlleur[] lEcranControlleur = new EcranControlleur[1];
	public static Main main;
	public static String lAdresse;

	private Humain enfantChoisit;
	private Salarie salarieChoisit;

	@FXML
	public GridPane fond;
	@FXML
	public VBox derriereCbChoixVille;

	public ToggleGroup sexeDeLEmploye;
	public ToggleGroup sexeDeLAffichage;
	public ToggleGroup sexeDeLEnfant;
	@FXML
	public VBox vbDroits;
	@FXML
	public VBox vbEnfants;
	@FXML
	public HBox hbChangeEmployer;
	@FXML
	public HBox hbChangeEnfant;
	// - g�che-----------------------------------------------------------------
	@FXML
	public ComboBox<String> cbChoixVille;
	@FXML
	public ListView<String> lvEmploi;
	@FXML
	public Label labelPersonneChoisie;
	@FXML
	public Label labelRestaurant;
	@FXML
	public Label labelNoel;
	@FXML
	public Label labelVacances;
	@FXML
	public TextField txtfieNom;
	@FXML
	public TextField txtfiePrenom;
	@FXML
	public DatePicker dpAdulte;
	@FXML
	public RadioButton radioFemme;
	@FXML
	public RadioButton radioHomme;
	@FXML
	public Button btnEmbauche;
	@FXML
	public Button btnEmploiDel;
	// - Drouate---------------------------------------------------------------
	@FXML
	public RadioButton radioAfficherHomme;
	@FXML
	public RadioButton radioAfficherFemme;
	@FXML
	public RadioButton radioAfficherTout;
	@FXML
	public CheckBox checkSansBonus;
	@FXML
	public CheckBox checkVacances;
	@FXML
	public CheckBox checkNoel;
	@FXML
	public Button btnImprimer;
	@FXML
	public ListView<String> lvEnfants;
	@FXML
	public TextField txtfiePrenomEnfant;
	@FXML
	public DatePicker dpEnfant;
	@FXML
	public RadioButton radioFille;
	@FXML
	public RadioButton radioGarcon;
	@FXML
	public Button btnAjouteEnfant;
	@FXML
	public Button btnEnfantDel;

	// - Getteurs/Setteurs-----------------------------------------------------

	public static EcranControlleur[] getlEcranControlleur()
	{
		return lEcranControlleur;
	}

	public static void setlEcranControlleur(EcranControlleur[] lEcranControlleur)
	{
		EcranControlleur.lEcranControlleur = lEcranControlleur;
	}

	// - Chooser---------------------------------------------------------------
	private void setLeSalarier(String selectedItem)
	{
		if (selectedItem.equals("Ajouter Salarier"))
		{
			txtfieNom.setText("");
			txtfiePrenom.setText("");
			dpAdulte.setValue(null);
			radioHomme.setSelected(false);
			radioFemme.setSelected(false);
			lvEnfants.setItems(null);
			txtfiePrenomEnfant.setText("");
			radioFille.setSelected(false);
			radioGarcon.setSelected(false);
			dpEnfant.setValue(null);
		}
		else
		{
			for (Filliales laFilliales : Filliales.getLesFilliales())
			{
				for (Salarie laPersonne : laFilliales.getSalariesEmbaucher())
				{
					if (laPersonne.toString().equals(selectedItem))
					{
						salarieChoisit = laPersonne;
					}
				}
			}
			txtfieNom.setText(salarieChoisit.getNom());
			txtfiePrenom.setText(salarieChoisit.getPrenom());
			dpAdulte.setValue(salarieChoisit.getDateDeNaissance());
			if (salarieChoisit.isFemme())
				radioFemme.setSelected(true);
			else
				radioHomme.setSelected(true);
		}
	}

	private void setLEnfant(String selectedItem)
	{
		if (selectedItem.equals("Ajouter enfant"))
		{
			txtfiePrenomEnfant.setText("");
			dpEnfant.setValue(null);
			radioFille.setSelected(false);
			radioGarcon.setSelected(false);
		}
		else
		{
			for (Humain lEnfant : salarieChoisit.getEnfants())
			{
				if (lEnfant.getPrenom().equals(selectedItem))
				{
					enfantChoisit = lEnfant;
				}
			}
			txtfiePrenomEnfant.setText(enfantChoisit.getPrenom());
			dpEnfant.setValue(enfantChoisit.getDateDeNaissance());
			if (enfantChoisit.isFemme())
				radioFille.setSelected(true);
			else
				radioGarcon.setSelected(true);
		}
	}

	private List<String> attraperEnfants(List<String> laListeDEnfant)
	{
		for (Humain lEnfant : salarieChoisit.getEnfants())
			laListeDEnfant.add(lEnfant.toString());
		return laListeDEnfant;
	}

	// - Activeurs-------------------------------------------------------------
	public void activerTout(boolean actif)
	{
		final ObservableList<Node> children = fond.getChildren();
		for (Node laNode : children)
		{
			laNode.setDisable(!actif);
		}
		lvEmploi.setDisable(!actif);
		activerDroits(actif);
	}

	public void activerPersonne(boolean actif)
	{
		activerDroits(actif);
		vbEnfants.setDisable(!actif);
		hbChangeEmployer.setDisable(!actif);
	}

	public void activerNouvellePersonne(boolean actif)
	{
		hbChangeEmployer.setDisable(!actif);
	}

	public void activerEnfant(boolean actif)
	{
		vbEnfants.setDisable(!actif);
		hbChangeEnfant.setDisable(!actif);
	}

	private void activerDroits(boolean actif)
	{
		if (!actif)
		{
			labelRestaurant.setText("");
			labelNoel.setText("");
			labelVacances.setText("");
		}
		vbDroits.setDisable(!actif);
	}

	// - Initialisation et actions globales------------------------------------
	@FXML
	private void initialize()
	{
		lEcranControlleur[0] = this;
		Main.setEcranControlleur(this);
		activerTout(false);
		sexeDeLEmploye = new ToggleGroup();
		sexeDeLEnfant = new ToggleGroup();
		sexeDeLAffichage = new ToggleGroup();
		radioFemme.setToggleGroup(sexeDeLEmploye);
		radioHomme.setToggleGroup(sexeDeLEmploye);
		radioFille.setToggleGroup(sexeDeLEnfant);
		radioGarcon.setToggleGroup(sexeDeLEnfant);
		radioAfficherHomme.setToggleGroup(sexeDeLAffichage);
		radioAfficherFemme.setToggleGroup(sexeDeLAffichage);
		radioAfficherTout.setToggleGroup(sexeDeLAffichage);
	}

	@FXML
	public void quitter()
	{
		Platform.exit();
	}

	@FXML
	public void charger()
	{
		activerTout(false);
		SalarieLoader.getSalarier(Filliales.getLesFilliales());
	}

	// - cuisine interne-------------------------------------------------------
	public static void setAdresse(String uneAdresse)
	{
		lAdresse = uneAdresse;
		Main.getPrimaryStage().setTitle("Lecture de la liste " + lAdresse);
	}

	public static void setMain(Main leMain)
	{
		main = leMain;
	}

	// - gestion selection-----------------------------------------------------
	public static void quandElementChargerStatic()
	{
		setAdresse(lAdresse);
		lEcranControlleur[0].quandElementChargerNonStatic();
	}

	private void quandElementChargerNonStatic()
	{
		if (!Filliales.getLesFilliales().isEmpty())
		{
			List<String> laListeVille = new ArrayList<>();
			laListeVille.add("Afficher tout");
			for (Filliales laFilliale : Filliales.getLesFilliales())
			{
				laListeVille.add(laFilliale.toString());
			}
			cbChoixVille.setItems(FXCollections.observableList(laListeVille));

			cbChoixVille.setDisable(false);
			derriereCbChoixVille.setDisable(false);

			cbChoixVille.setOnAction(event -> {
				if (cbChoixVille.getValue() != null)
				{
					setVille(cbChoixVille.getValue());
					activerTout(true);
					activerPersonne(false);
					activerEnfant(false);
				}
			});

		}
	}

	protected void setVille(String string)
	{
		List<String> laListe = new ArrayList<>();
		remplisseurString(string, laListe);
		lvEmploi.setItems(FXCollections.observableList(laListe));
		lvEmploi.setOnMouseClicked(event -> salarieChoisit(lvEmploi.getSelectionModel().getSelectedItem()));
	}

	private void salarieChoisit(String selectedItem)
	{
		if (selectedItem.equals("Ajouter Salarier"))
		{
			setLeSalarier(selectedItem);
			labelPersonneChoisie.setText("Choisir un(e) employ�(e)");
			activerPersonne(false);
			activerNouvellePersonne(true);
			btnEmbauche.setText("Embaucher");
			btnEmploiDel.setDisable(true);
		}
		else
		{
			setLeSalarier(selectedItem);
			activerPersonne(true);
			setDroits(selectedItem);
			btnEmbauche.setText("Modifier");

			List<String> laListeDEnfant = new ArrayList<>();
			laListeDEnfant.add("Ajouter enfant");
			attraperEnfants(laListeDEnfant);
			lvEnfants.setItems(FXCollections.observableList(laListeDEnfant));
			lvEnfants.setOnMouseClicked(event -> enfantChoisit(lvEnfants.getSelectionModel().getSelectedItem()));
			btnEmploiDel.setDisable(false);
		}
	}

	private void setDroits(String selectedItem)
	{
		labelPersonneChoisie.setText("Vous avez s�l�ctionn�: " + selectedItem);
		int[] lesDroits = checkdroits();
		labelNoel.setText("- " + lesDroits[0] + " � de ch�ques no�l");
		if (lesDroits[1] == 0)
			labelVacances.setText("- N'as pas de ch�ques vacances");
		else
			labelVacances.setText("- � des ch�ques vacances");
	}

	private int[] checkdroits()
	{
		Filliales laFillialeActuelle = null;
		for (Filliales filliale : Filliales.getLesFilliales())
		{
			if (salarieChoisit.getAgence().equals(filliale.toString()))
				laFillialeActuelle = filliale;
		}
		if (laFillialeActuelle != null)
		{
			if (laFillialeActuelle.isAsUnRestaurant())
				labelRestaurant.setText("- Restaurant interne");
			else
				labelRestaurant.setText("- Tickets restaurants");
		}
		else
			labelRestaurant.setText("- Erreur r�cup�ration restaurant");

		return salarieChoisit.calculerMesDroits();
	}

	private void enfantChoisit(String selectedItem)
	{
		if (selectedItem.equals("Ajouter enfant"))
		{
			setLEnfant(selectedItem);
			activerEnfant(true);
			btnAjouteEnfant.setText("Ajouter l'enfant");
			btnEnfantDel.setDisable(true);
		}
		else
		{
			setLEnfant(selectedItem);
			activerEnfant(true);
			btnAjouteEnfant.setText("Changer l'enfant");
			btnEnfantDel.setDisable(false);
		}
	}

	// - gestion affichage-----------------------------------------------------
	@FXML
	private void changeAffichage()
	{
		setVille(cbChoixVille.getValue());
		activerPersonne(false);
	}

	private void remplisseurString(String string, List<String> laListe)
	{
		int sexe = radioAffichageCheck();
		boolean pasDeBonus = checkSansBonus.isSelected();
		boolean bonusVacances = checkVacances.isSelected();
		boolean bonusNoel = checkNoel.isSelected();

		for (Filliales laFilliale : Filliales.getLesFilliales())
		{
			if (string.equals("Afficher tout"))
			{
				remplisseurStringAfficherTout(laListe, sexe, pasDeBonus, bonusVacances, bonusNoel, laFilliale);

			}
			if (laFilliale.getLeNom().equals(string))
			{
				remplisseurStringAfficherSpecifique(laListe, sexe, pasDeBonus, bonusVacances, bonusNoel, laFilliale);
			}
		}
	}

	private void remplisseurStringAfficherSpecifique(List<String> laListe, int sexe, boolean pasDeBonus,
			boolean bonusVacances, boolean bonusNoel, Filliales laFilliale)
	{
		boolean sexeOK;
		boolean bonusOK;
		Filliales fillialeChoisit;
		fillialeChoisit = laFilliale;
		laListe.add("Ajouter Salarier");
		for (int i = 0; i < fillialeChoisit.getSalariesEmbaucher().size(); i++)
		{
			sexeOK = ((laFilliale.getSalariesEmbaucher().get(i).isFemme() && sexe == 1)
					|| (!laFilliale.getSalariesEmbaucher().get(i).isFemme() && sexe == 2) || sexe == 0);
			bonusOK = ((laFilliale.getSalariesEmbaucher().get(i).calculerMesDroits()[0] == 0)
					&& (laFilliale.getSalariesEmbaucher().get(i).calculerMesDroits()[1] == 0) && pasDeBonus);
			bonusOK = bonusOK || ((laFilliale.getSalariesEmbaucher().get(i).calculerMesDroits()[0] != 0) && bonusNoel);
			bonusOK = bonusOK
					|| ((laFilliale.getSalariesEmbaucher().get(i).calculerMesDroits()[1] != 0) && bonusVacances);
			if (sexeOK && bonusOK)
			{
				laListe.add(fillialeChoisit.getSalariesEmbaucher().get(i).toString());
			}
		}
	}

	private void remplisseurStringAfficherTout(List<String> laListe, int sexe, boolean pasDeBonus,
			boolean bonusVacances, boolean bonusNoel, Filliales laFilliale)
	{
		boolean sexeOK;
		boolean bonusOK;
		for (int i = 0; i < laFilliale.getSalariesEmbaucher().size(); i++)
		{
			sexeOK = ((laFilliale.getSalariesEmbaucher().get(i).isFemme() && sexe == 1)
					|| (!laFilliale.getSalariesEmbaucher().get(i).isFemme() && sexe == 2) || sexe == 0);
			bonusOK = ((laFilliale.getSalariesEmbaucher().get(i).calculerMesDroits()[0] == 0)
					&& (laFilliale.getSalariesEmbaucher().get(i).calculerMesDroits()[1] == 0) && pasDeBonus);
			bonusOK = bonusOK || ((laFilliale.getSalariesEmbaucher().get(i).calculerMesDroits()[0] != 0) && bonusNoel);
			bonusOK = bonusOK
					|| ((laFilliale.getSalariesEmbaucher().get(i).calculerMesDroits()[1] != 0) && bonusVacances);
			if (sexeOK && bonusOK)
			{
				laListe.add(laFilliale.getSalariesEmbaucher().get(i).toString());
			}
		}
	}

	private int radioAffichageCheck()
	{
		if (radioAfficherHomme.isSelected())
		{
			return 2;
		}
		else if (radioAfficherFemme.isSelected())
		{
			return 1;
		}
		radioAfficherTout.setSelected(true);
		return 0;
	}

	// - Ajouter/Supprimer/changer personne------------------------------------
	@FXML
	public void boutonSupprimeEmployer()
	{
		for (Filliales laFilliale : Filliales.getLesFilliales())
		{
			if (salarieChoisit.getAgence().equals(laFilliale.toString()))
			{
				Alert alert = new Alert(AlertType.CONFIRMATION, "Voulez vous vraiment supprimer : "
						+ salarieChoisit.getNom() + "  " + salarieChoisit.getPrenom());
				Optional<ButtonType> result = alert.showAndWait();
				if (result.isPresent() && result.get() == ButtonType.OK)
				{
					laFilliale.getSalariesEmbaucher().remove(salarieChoisit);
					setLeSalarier("Ajouter Salarier");
					changeAffichage();
				}
			}
		}
	}

	@FXML
	public void changePersonne()
	{
		if (lvEmploi.getSelectionModel().getSelectedItem().equals("Ajouter Salarier"))
		{
			ajouterSalarier();

		}
		else
		{
			changerSalarier();
		}
	}

	private void changerSalarier()
	{
		String errorText = "";
		boolean isOk = true;
		boolean isFemme = true;

		if (txtfieNom.getText().isBlank())
		{
			isOk = false;
			errorText = errorText + "Entrez un nom.\n";
		}
		if (txtfiePrenom.getText().isBlank())
		{
			isOk = false;
			errorText = errorText + "Entrez un prenom.\n";
		}
		if ((dpAdulte.getValue() == null) || (dpAdulte.getValue()
				.compareTo(LocalDate.of(LocalDate.now().getYear() - 16, LocalDate.now().getMonth(), 1)) >= 0))
		{
			isOk = false;
			errorText = errorText + "La personne doit avoir au moins 18 ans.\n";
		}
		if (!radioFemme.isSelected() && !radioHomme.isSelected())
		{
			isOk = false;
			errorText = errorText + "Selectionnez un sexe.\n";
		}
		isFemme = !radioHomme.isSelected();

		if (!isOk)
		{
			Alert alert = new Alert(AlertType.INFORMATION, errorText);
			alert.showAndWait();
		}
		else
		{
			wasOkEmployerChanger(isFemme);
		}
	}

	private void wasOkEmployerChanger(boolean isFemme)
	{
		salarieChoisit.setFemme(isFemme);
		salarieChoisit.setNom(txtfieNom.getText());
		salarieChoisit.setPrenom(txtfiePrenom.getText());
		salarieChoisit.setDateDeNaissance(dpAdulte.getValue());

		changeAffichage();
		setLeSalarier(salarieChoisit.toString());
	}

	private void ajouterSalarier()
	{
		String errorText = "";
		boolean isOk = true;
		boolean isFemme = true;

		if (txtfieNom.getText().isBlank())
		{
			isOk = false;
			errorText = errorText + "Entrez un nom.\n";
		}
		if (txtfiePrenom.getText().isBlank())
		{
			isOk = false;
			errorText = errorText + "Entrez un prenom.\n";
		}
		if ((dpAdulte.getValue() == null) || (dpAdulte.getValue()
				.compareTo(LocalDate.of(LocalDate.now().getYear() - 16, LocalDate.now().getMonth(), 1)) >= 0))
		{
			isOk = false;
			errorText = errorText + "La personne doit avoir au moins 18 ans.\n";
		}
		if (!radioFemme.isSelected() && !radioHomme.isSelected())
		{
			isOk = false;
			errorText = errorText + "Selectionnez un sexe.\n";
		}
		isFemme = !radioHomme.isSelected();

		if (!isOk)
		{
			Alert alert = new Alert(AlertType.INFORMATION, errorText);
			alert.showAndWait();
		}
		else
		{
			wasOkEmployer(isFemme);
		}
	}

	private void wasOkEmployer(boolean isFemme)
	{
		for (Filliales laFilliale : Filliales.getLesFilliales())
		{
			if (laFilliale.toString().equals(cbChoixVille.getValue()))
			{
				laFilliale.getSalariesEmbaucher().add(new Salarie(isFemme, txtfiePrenom.getText(), dpAdulte.getValue(),
						txtfieNom.getText(), laFilliale.toString(), LocalDate.now()));
				changeAffichage();
				setLeSalarier(
						laFilliale.getSalariesEmbaucher().get(laFilliale.getSalariesEmbaucher().size() - 1).toString());
				lvEmploi.scrollTo(laFilliale.getSalariesEmbaucher().size());
				lvEmploi.getSelectionModel().select(laFilliale.getSalariesEmbaucher().size());
			}
		}
	}

	// - Ajouter/Supprimer/changer enfant--------------------------------------
	@FXML
	public void boutonSupprimerEnfant()
	{
		Alert alert = new Alert(AlertType.CONFIRMATION,
				"Voulez vous vraiment supprimer : " + salarieChoisit.getNom() + "  " + enfantChoisit.getPrenom());
		Optional<ButtonType> result = alert.showAndWait();
		if (result.isPresent() && result.get() == ButtonType.OK)
		{
			salarieChoisit.getEnfants().remove(enfantChoisit);
			setLEnfant("Ajouter enfant");
			salarieChoisit(salarieChoisit.toString());
			activerEnfant(false);
		}
	}

	@FXML
	public void changeEnfant()
	{
		if (lvEnfants.getSelectionModel().getSelectedItem().equals("Ajouter enfant"))
		{
			ajouterEnfant();

		}
		else
		{
			changerEnfant();
		}
	}

	private void changerEnfant()
	{
		String errorText = "";
		boolean isOk = true;
		boolean isFemme = true;
		boolean dateNaissanceOk = true;
		
		if (txtfiePrenomEnfant.getText().isBlank())
		{
			isOk = false;
			errorText = errorText + "Entrez un prenom.\n";
		}
		dateNaissanceOk = Salarie.checkDatePossibleEnfant(salarieChoisit, dpEnfant.getValue());
		if (!dateNaissanceOk)
		{
			isOk = false;
			errorText = errorText + "Entrez une date de naissance valide\n";
		}
		if (!radioFille.isSelected() && !radioGarcon.isSelected())
		{
			isOk = false;
			errorText = errorText + "Selectionnez un sexe.\n";
		}
		isFemme = !radioGarcon.isSelected();

		if (!isOk)
		{
			Alert alert = new Alert(AlertType.INFORMATION, errorText);
			alert.showAndWait();
		}
		else
		{
			wasOkEnfantChanger(isFemme);
		}
	}

	private void wasOkEnfantChanger(boolean isFemme)
	{
		enfantChoisit.setFemme(isFemme);
		enfantChoisit.setPrenom(txtfiePrenomEnfant.getText());
		enfantChoisit.setDateDeNaissance(dpEnfant.getValue());

		changeAffichage();
		setLEnfant(enfantChoisit.toString());
	}

	private void ajouterEnfant()
	{
		String errorText = "";
		boolean isOk = true;
		boolean isFemme = true;
		boolean dateNaissanceOk = true;

		if (txtfiePrenomEnfant.getText().isBlank())
		{
			isOk = false;
			errorText = errorText + "Entrez un prenom.\n";
		}
		dateNaissanceOk = Salarie.checkDatePossibleEnfant(salarieChoisit, dpEnfant.getValue());
		if (!dateNaissanceOk)
		{
			isOk = false;
			errorText = errorText + "Entrez une date de naissance valide\n";
		}
		if (!radioFille.isSelected() && !radioGarcon.isSelected())
		{
			isOk = false;
			errorText = errorText + "Selectionnez un sexe.\n";
		}
		isFemme = !radioGarcon.isSelected();

		if (!isOk)
		{
			Alert alert = new Alert(AlertType.INFORMATION, errorText);
			alert.showAndWait();
		}
		else
		{
			wasOkEnfant(isFemme);
		}
	}

	private void wasOkEnfant(boolean isFemme)
	{
		salarieChoisit.getEnfants().add(new Humain(isFemme, txtfiePrenomEnfant.getText(), dpEnfant.getValue()));
		changeAffichage();
		lvEnfants.scrollTo(salarieChoisit.getEnfants().size());
		lvEnfants.getSelectionModel().select(salarieChoisit.getEnfants().size());
		salarieChoisit(salarieChoisit.toString());
	}

	// - Impression------------------------------------------------------------
	@FXML
	private void imprimer() 
	{
		Imprimeur.imprime(lvEmploi, cbChoixVille.getValue());
	}

	// - Afficher aide---------------------------------------------------------
	@FXML
	private void aide()
	{
		Alert alert = new Alert(AlertType.INFORMATION, "Cr�er par un asticot qui rampe sur un clavier.");
		alert.showAndWait();
	}
}