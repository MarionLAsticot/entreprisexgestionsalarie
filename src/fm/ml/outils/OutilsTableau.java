package fm.ml.outils;

public abstract class OutilsTableau
{
	private OutilsTableau() {}
	
	/**
	 * Cherche dans le tableau donn� en argument, <br>
	 * et retourne la premi�re position vide.
	 */
	public static int premierePlaceLibre(Object[] tableau) 
	{
		int valeur = -1;
		for (int i = 0; i < tableau.length && valeur == -1 ;i++)
		{
			if (tableau[i] == null)
				valeur = i;
		}
		
		return valeur;
	}
	
	/**
	 * afficherContenuListe(La liste a parcourir, ce que la liste devrait contenir) <br>
	 * Affiche tout les objets dans la liste pass� en argument. <br>
	 * S'il n'y � rien, affiche "il n'y � pas" suivit du String.
	 */
	public static void afficherContenuListe(Object[] lesObjets, String ceQuIlNYAPas)
	{
		boolean estVide = true;
		for (int i = 0; i < lesObjets.length; i++)
		{
			if (lesObjets[i] != null)
			{
				System.out.println(lesObjets[i]);
				estVide = false;
			}
		}
		if (estVide)
			System.out.printf("%nIl n'y � pas %s.%n%n", ceQuIlNYAPas);
		OutilsSaisie.attendre();
	}
}
