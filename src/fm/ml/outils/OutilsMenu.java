package fm.ml.outils;

public abstract class OutilsMenu
{
	private OutilsMenu()
	{

	}

	/**
	 * Affiche un menu comprenant l'option "Quitter" en zero,<br>
	 * qui vide l'�cran avant, dont le nom du menu est le string,<br>
	 * et dont les proposition sont les occurences de la liste.
	 */
	public static int afficherMenu(String titre, Object[] options)

	{
		return afficherMenu(titre, options, "Quitter", true);
	}
	
	/**
	 * Affiche un menu comprenant en zero la proposition du troisi�me parametre,<br>
	 * qui vide l'�cran avant selon le bool�en en dernier parametre, dont le nom du menu est le string,<br>
	 * et dont les proposition sont les occurences de la liste.
	 */
	public static int afficherMenu(String titre, Object[] options, String choixZero, boolean ecranVide)
	{
		int leChoix = -1;

		if (ecranVide)
		{
			OutilsSaisie.clearScreen();
		}

		do
		{

			System.out.println(titre);

			for (int i = 0; i < titre.length(); i++)
				System.out.print("=");
			System.out.println();

			System.out.println(0 + " - " + choixZero);

			for (int i = 0; i < options.length; i++)
			{
				if (options[i] != null)
					System.out.println(i + 1 + " - " + options[i]);
			}

			leChoix = OutilsSaisie.scanEntier("%n", "L'entr�e n'est pas un chiffre");

		}
		while (leChoix < 0 || leChoix >= options.length + 1);
		OutilsSaisie.clearScreen();
		return leChoix;
	}
	
	/**
	 * Affiche une liste, et supprime tout les �l�ments s�l�ctionn�.
	 */
	public static Object[] menuSuppression(Object[] laListe)
	{
		int aSupprimer = 0;

		do
		{
			aSupprimer = OutilsMenu.afficherMenu("Que supprimer? ", laListe);

			if (laListe.length > 0)
			{
				if (aSupprimer > 0 && aSupprimer < laListe.length)
				{
					for (int i = aSupprimer; i < laListe.length - 1; i++)
					{
						laListe[i - 1] = laListe[i];
					}
					laListe[laListe.length - 1] = null;
				}
			}
			else
			{
				aSupprimer = 0;
			}
		}
		while (aSupprimer != 0);
		return laListe;
	}
}
