package fm.ml.outils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public abstract class OutilsGerantFichier
{
	private OutilsGerantFichier()
	{
	}

	public static List<String> chargerFichier(Stage stage, String leString)
	{
		String leFile = utiliserFileChooser(stage, "C:\\");

		ArrayList<String> lignes = new ArrayList<>();
		if (leFile.equals(""))
		{
			lignes.add(leString);
			return lignes;
		}

		if (leFile != null)
		{
			try (BufferedReader fichier = new BufferedReader(new FileReader(leFile)))

			{
				String ligne;
				while ((ligne = fichier.readLine()) != null)
				{
					lignes.add(ligne);
				}
			}
			catch (IOException e)
			{
				System.out.println("inateignable");
			}
		}
		return lignes;
	}

	/**
	 * Retourne une liste comprennant:<br/>
	 * l'uri en tout premier �l�ment<br/>
	 * puis le contenu du texte<br/>
	 * @param stage
	 * @param leString
	 * @return
	 */
	public static List<String> chargerFichierEtUri(Stage stage, String leString)
	{
		String leFile = utiliserFileChooser(stage, "C:\\");

		ArrayList<String> lignes = new ArrayList<>();
		if (leFile.equals(""))
		{
			lignes.add(leString);
			return lignes;
		}
		
		if (leFile != null)
		{
			lignes.add(leFile);
			try (BufferedReader fichier = new BufferedReader(new FileReader(leFile)))

			{
				String ligne;
				while ((ligne = fichier.readLine()) != null)
				{
					lignes.add(ligne);
				}
			}
			catch (IOException e)
			{
				System.out.println("inateignable");
			}
		}
		return lignes;
	}
	
	public static void sauverFichier(String leString, String leFile)
	{
		if (leFile != null)
		{
			try (BufferedWriter fichier = new BufferedWriter(new FileWriter(leFile, false)))
			{
				fichier.append(leString);
			}
			catch (IOException e)
			{
				System.out.println("inateignable");
			}
		}
	}

	public static void sauverSousFichier(Stage stage, String leString)
	{
		String leFile = utiliserFileChooser(stage, "C:\\");

		if (leFile != null)
		{
			try (BufferedWriter fichier = new BufferedWriter(new FileWriter(leFile, false)))
			{
				fichier.append(leString);
			}
			catch (IOException e)
			{
				System.out.println("inateignable");
			}
		}
	}

	private static String utiliserFileChooser(Stage stage, String uri)
	{
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("chargeur");
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("textes", "*.txt"));
		fileChooser.setInitialDirectory(new File(uri));
		try
		{
			return fileChooser.showOpenDialog(stage).getAbsolutePath();
		}
		catch (Exception e)
		{
			return "";
		}
	}

}
