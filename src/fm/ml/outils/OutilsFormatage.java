package fm.ml.outils;

public abstract class OutilsFormatage
{
	
	private OutilsFormatage() {}
	
	/**
	 * Renvoie la chaine de caract�re pass� en argument, mais <br>
	 * avec les espaces de d�but, de fin, et les doubles espaces <br>
	 * de retir�.
	 */
	public static String retirerEspacesEnTrop(String leTexte)
	{
		leTexte = leTexte.trim();
		int i = 0;
		while (i != -1)
		{
			i = leTexte.indexOf("  ");
			if (i != -1)
				leTexte = leTexte.substring(0, i) + leTexte.substring(i + 1);
		}
		return leTexte;
	}

	/**
	 * Renvoie la m�me chaine de caract�res pass� en argument, <br>
	 * mais sans aucun caract�re accentu�s <br>
	 */
	public static String retirerAccents(String leTexte)
	{
		String avecAccents = "�����������������������������";
		String sansAccents = "saaaaaeeeeuuuuiiinfUAEEIIooooOO";
		int positionAccent = 0;
		for (int i = 0; i < leTexte.length(); i++)
		{
			positionAccent = avecAccents.indexOf(leTexte.substring(i, i + 1));
			if (positionAccent != -1)
				leTexte = leTexte.substring(0, i) + sansAccents.substring(positionAccent, positionAccent + 1)
						+ leTexte.substring(i + 1);
		}
		return leTexte;
	}
	
	/**
	 * Renvoie la m�me chaine de caract�res pass� en argument, <br>
	 * mais en minuscule <br>
	 */
	public static String garderTexteMinuscule(String leTexte)
	{
		String leNouveauTexte = "";
		for (int i = 0; i < leTexte.length()-1; i++)
		{
			if (!(leTexte.toLowerCase().substring(i, i+1).equals(leTexte.toUpperCase().substring(i, i+1))))
			{
				leNouveauTexte = leNouveauTexte + leTexte.toLowerCase().substring(i, i+1);
			}
		}
		return leNouveauTexte;
	}	
	
	public static String enNomPropre(String leTexte)
	{
		String leNouveauTexte = "";
		leNouveauTexte = leNouveauTexte + leTexte.toLowerCase().substring(0, 1);
		for (int i = 1; i < leTexte.length()-1; i++)
		{
			if (!(leTexte.toLowerCase().substring(i, i+1).equals(leTexte.toUpperCase().substring(i, i+1))))
			{
				leNouveauTexte = leNouveauTexte + leTexte.toLowerCase().substring(i, i+1);
			}
		}
		return leNouveauTexte;
	}

}
