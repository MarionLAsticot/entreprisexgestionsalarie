package fm.ml.outils;

import java.util.Scanner;

public abstract class OutilsSaisie
{
	private OutilsSaisie()
	{
	}

	static Scanner monScan = new Scanner(System.in);

	/**
	 * Demande � l'utilisateur un entier.<br>
	 * Boucle tant que la saisie est incorrecte.
	 */
	public static int scanEntier()
	{

		System.out.println("");
		boolean vrai = false;
		int resultat = 0;
		do
			try
			{
				vrai = true;
				resultat = Integer.parseInt(monScan.nextLine());
			}
			catch (Exception e)
			{
				vrai = false;
				System.out.printf("%nRecommencez la saisie, n'entrez qu'un nombre.%n%n");
			}
		while (!vrai);
		System.out.println("");
		return resultat;
	}

	/**
	 * Demande � l'utilisateur un entier.<br>
	 * Affiche le contenu du premier String � chaque saisie demandes de saisies<br>
	 * Boucle tant que la saisie est incorrecte.
	 */
	public static int scanEntier(String texte)
	{
		boolean vrai = false;
		int resultat = 0;
		do
			try
			{
				System.out.printf(texte);
				System.out.println("");
				vrai = true;
				resultat = Integer.parseInt(monScan.nextLine());
			}
			catch (Exception e)
			{
				vrai = false;
				System.out.printf("%nRecommencez la saisie, n'entrez qu'un nombre.%n%n");
			}
		while (!vrai);
		System.out.println("");
		return resultat;

	}

	/**
	 * Demande � l'utilisateur un entier.<br>
	 * Affiche le contenu du premier String � chaque saisie demandes de saisies<br>
	 * Affiche le contenu du second String � chaque saisie fausse<br>
	 * Boucle tant que la saisie est incorrecte.
	 */
	public static int scanEntier(String texte, String erreur)

	{
		boolean vrai = false;
		int resultat = 0;
		do
			try
			{
				System.out.printf(texte);
				System.out.println("");
				vrai = true;
				resultat = Integer.parseInt(monScan.nextLine());
			}
			catch (Exception e)
			{
				vrai = false;
				System.out.printf(erreur);
			}
		while (!vrai);
		System.out.println("");
		return resultat;

	}

	/**
	 * Demande � l'utilisateur un entier.<br>
	 * Affiche le contenu du premier String � chaque saisie demandes de saisies<br>
	 * Affiche le contenu du second String � chaque saisie fausse<br>
	 * Boucle tant que la saisie est incorrecte, et n'est pas entre les deux int.
	 */
	public static int scanEntier(String texte, String erreur, int min, int max)

	{
		if (min > max)
		{
			int echange = min;
			min = max;
			max = echange;
		}
		boolean vrai = false;
		int resultat = 0;
		do
			try
			{
				System.out.printf(texte);
				System.out.println("");
				vrai = true;
				resultat = Integer.parseInt(monScan.nextLine());
				if (resultat < min || resultat > max)
				{
					vrai = false;
					System.out.printf(erreur);
					System.out.println("");
				}
			}
			catch (Exception e)
			{
				vrai = false;
				System.out.printf(erreur);
				System.out.println("");
			}
		while (!vrai);
		System.out.println("");
		return resultat;

	}

	/**
	 * Demande � l'utilisateur un String.<br>
	 * Affiche "Entrez un texte" � chaque saisie vide<br>
	 * Boucle tant que la saisie est incorrecte.
	 */
	public static String scanString()
	{
		System.out.println("");
		boolean vrai = false;
		String resultat = "";
		do
		{
			vrai = true;
			resultat = (monScan.nextLine());
			if (resultat.equalsIgnoreCase(""))
			{
				System.out.println("Entrez un texte.");
				vrai = false;
			}
		}
		while (!vrai);
		System.out.println("");
		return resultat;
	}

	/**
	 * Demande � l'utilisateur un bool�en.<br>
	 * Accepte "Oui", "Vrai" et "True" pour True<br>
	 * Accepte "Non", "Faux" et "False" pour faux<br>
	 * Ignore la case<br>
	 * Boucle tant que la saisie est incorrecte.
	 */

	public static boolean scanBooleen()
	{
		System.out.println("");
		boolean vrai = false;
		boolean choix = false;
		String resultat = "";

		do
		{
			vrai = true;
			resultat = (monScan.nextLine());
			if (!resultat.equalsIgnoreCase("true") && !resultat.equalsIgnoreCase("false")
					&& !resultat.equalsIgnoreCase("vrai") && !resultat.equalsIgnoreCase("faux")
					&& !resultat.equalsIgnoreCase("oui") && !resultat.equalsIgnoreCase("non"))
			{
				System.out.println("Entrez un booleen (\"vrai\" ou \"faux\").");
				vrai = false;
			}

			if (resultat.equalsIgnoreCase("true") || resultat.equalsIgnoreCase("vrai")
					|| resultat.equalsIgnoreCase("oui"))
				choix = vrai;
		}
		while (!vrai);
		System.out.println("");
		return choix;
	}

	/**
	 * Attend que l'utilisateur renvoie un texte<br>
	 * Affiche en demande le texte indiqu� en argument.<br>
	 * Boucle tant que la saisie est incorrecte.
	 */
	public static String scanString(String texte)
	{
		boolean vrai = false;
		String resultat = "";
		do
		{
			System.out.println(texte);
			System.out.println("");
			vrai = true;
			resultat = (monScan.nextLine());
			if (resultat.equalsIgnoreCase(""))
			{
				System.out.println("Entrez un texte.");
				vrai = false;
			}
		}
		while (!vrai);
		System.out.println("");
		return resultat;
	}

	/**
	 * 
	 * Affiche le texte format� donn� en argument. <br>
	 * Retourne un character, le premier character �crit au clavi�.<br>
	 * Boucle tant que la saisie est incorrecte.
	 */
	public static char scanNextChar(String texte)
	{
		boolean vrai = false;
		String resultat = "";
		do
		{
			System.out.printf(texte);
			vrai = true;
			resultat = (monScan.nextLine());
			resultat = OutilsFormatage.retirerEspacesEnTrop(resultat);
			if (resultat.equalsIgnoreCase("")
					|| (Character.toLowerCase(resultat.charAt(0)) == Character.toUpperCase(resultat.charAt(0))))
			{
				System.out.println("Entrez une lettre.");
				vrai = false;
			}
		}
		while (!vrai);
		System.out.println("");
		return resultat.charAt(0);
	}

	/**
	 * Attend jusqu'� ce que l'utilisateur appuis sur entr�e. <br>
	 * Rien n'est enregistr�.
	 */
	public static void attendre()
	{
		System.out.println("");
		System.out.println("Appuyez sur entr�e pour continuer. ");
		System.out.println("");
		monScan.nextLine();
	}

	/**
	 * Attend jusqu'� ce que l'utilisateur appuis sur entr�e. <br>
	 * Rien n'est enregistr�. Affiche ou non un texte selon le booleen
	 */
	public static void attendre(boolean estAffiche)
	{
		System.out.println("");
		if (estAffiche)
		{
			System.out.println("Appuyez sur entr�e pour continuer. ");
			System.out.println("");
		}
		monScan.nextLine();
	}

	/**
	 * Rend l'�cran tout blanc.
	 */
	public static void clearScreen()
	{
		for (int i = 0; i < 50; i++)
			System.out.println("");
	}
}
